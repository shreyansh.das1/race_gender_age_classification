import torch.nn.functional as F
import torch
from constants import AGE_NODES, GENDER_NODES, RACE_NODES

class MTCNN(torch.nn.Module):
    """
    Creates a MTL model with the encoder from "backbone_arch"
    """
    def __init__(self, backbone_arch, dropout_rate):
        super(MTCNN,self).__init__()
        self.encoder = backbone_arch
        self.fc1 = torch.nn.Linear(1000,512)
        torch.nn.init.xavier_normal(self.fc1.weight)
        self.fc2 = torch.nn.Linear(512, 256)
        torch.nn.init.xavier_normal(self.fc2.weight)
        self.fc3 = torch.nn.Linear(256, 256)
        torch.nn.init.xavier_normal(self.fc3.weight)

        self.head_age = torch.nn.Linear(256, AGE_NODES)
        torch.nn.init.xavier_normal(self.head_age.weight)
        
        self.head_gender = torch.nn.Linear(256, GENDER_NODES)
        torch.nn.init.xavier_normal(self.head_gender.weight)

        self.head_race = torch.nn.Linear(256, RACE_NODES)
        torch.nn.init.xavier_normal(self.head_race.weight)

        self.dropout = torch.nn.Dropout(dropout_rate, inplace= True)

    def forward(self,x):

        x = self.encoder(x)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))

        age = F.softmax(self.head_age(x))
        gender = F.softmax(self.head_gender(x))
        race = F.softmax(self.head_race(x))

        return age, gender, race