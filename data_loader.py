import torch
from torch.utils.data import Dataset
import torch.nn.functional as F
from torchvision import transforms
from PIL import Image
import numpy as np

from constants import AGE_NODES, RACE_NODES, GENDER_NODES

class FairFaceDataset(Dataset):
    def __init__(self, df, transform):
        self.paths = list(df.file)
        self.age = list(df.age)
        self.gender = list(df.gender)
        self.race = list(df.race)
        self.transform = transform

    def __len__(self): 
        return len(self.paths)

    def __getitem__(self,idx):
        img = Image.open(self.paths[idx]).convert('RGB')
        img = self.transform(img)

        #dealing with the labels
        age = torch.tensor(int(self.age[idx]), dtype=torch.int64)
        age = F.one_hot(age, num_classes = AGE_NODES)
        gender = torch.tensor(int(self.gender[idx]), dtype=torch.int64)
        gender = F.one_hot(gender, num_classes = GENDER_NODES)
        race = torch.tensor(int(self.race[idx]), dtype=torch.int64)
        race = F.one_hot(race, num_classes = RACE_NODES)
        
        return img, (age, gender, race)