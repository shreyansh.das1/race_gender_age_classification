import torch
from torch import *
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import transforms, models
from torchsummary import summary
from data_loader import FairFaceDataset
from model import MTCNN
import pandas as pd
import wandb
import warnings
import time
import copy
from constants import BATCH_SIZE, NUM_EPOCHS, TRAINED_MODEL_FILE

warnings.filterwarnings("ignore")
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

wandb.init(project="race-gender-age-estimation", entity="shreyanshdas00", name="efficientnet-B7")
wandb.config = {
  "epochs": NUM_EPOCHS,
  "batch_size": BATCH_SIZE
}

def train_model(model, optimizer, scheduler, num_epochs=50):
    since = time.time()

    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc_age = 0
    best_acc_gender = 0
    best_acc_race = 0
    best_acc_avg = 0
    
    crossEntropyLoss = torch.nn.CrossEntropyLoss()

    for epoch in range(0, num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        metrics = {}

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            age_corrects = 0.0
            gender_corrects = 0.0
            race_corrects = 0.0
            
            # Iterate over data.
            for inputs, (age, gender, race) in dataloaders_dict[phase]:
                inputs = inputs.to(device) 
                
                age = age.to(device)
                gender = gender.to(device)
                race = race.to(device)
                
                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    outputs = model(inputs)
                    
                    #print(outputs[0],torch.max(age.float(), 1)[1])
                    
                    loss_age = crossEntropyLoss(outputs[0], torch.max(age.float(), 1)[1])
                    loss_gender = crossEntropyLoss(outputs[1], torch.max(gender.float(), 1)[1])
                    loss_race = crossEntropyLoss(outputs[2], torch.max(race.float(), 1)[1])
                    
                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss = loss_age + loss_gender + loss_race
                        #print(loss, loss0,loss1, loss2, loss3,loss4)
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                
                age_corrects += torch.sum(torch.max(outputs[0], 1)[1] == torch.max(age, 1)[1])
                gender_corrects += torch.sum(torch.max(outputs[1], 1)[1] == torch.max(gender, 1)[1])
                race_corrects += torch.sum(torch.max(outputs[2], 1)[1] == torch.max(race, 1)[1])
                
            if phase == "train":
                scheduler.step()
                
            #epoch_loss = running_loss / dataset_sizes[phase]
            age_acc = age_corrects.double() / dataset_sizes[phase]
            gender_acc = gender_corrects.double() / dataset_sizes[phase]
            race_acc = race_corrects.double() / dataset_sizes[phase]
            epoch_avg_acc = (race_acc+age_acc+gender_acc)/3
            
            print('{} total loss: {:.4f} age loss: {:.4f} gender loss: {:.4f} race loss: {:.4f} '.format(phase, loss, loss_age, loss_gender, loss_race))
            print('{} age_acc: {:.4f} gender_acc: {:.4f}  race_acc: {:.4f} '.format(phase, age_acc, gender_acc, race_acc))
            
            metrics = {
                **metrics, 
                "{}".format(phase): {
                "total_loss": loss, 
                "age_loss": loss_age, 
                "gender_loss": loss_gender, 
                "race_loss": loss_race, 
                "average_accuracy": epoch_avg_acc, 
                "age_accuracy": age_acc, 
                "gender_accuracy": gender_acc, 
                "race_accuracy": race_acc
                }}
            

            # deep copy the model
            if phase == 'val' and ((int(age_acc >= best_acc_age) + int(gender_acc >= best_acc_gender) + int(race_acc >= best_acc_race)) >= 2) and (epoch_avg_acc > best_acc_avg):
                print('saving with accuracies of age_acc: {:.4f} gender_acc: {:.4f}  race_acc: {:.4f} '.format(age_acc, gender_acc, race_acc), 'improved over previous age_acc: {:.4f} gender_acc: {:.4f}  race_acc: {:.4f} '.format(best_acc_age, best_acc_gender, best_acc_race))
                best_acc_age = age_acc
                best_acc_gender = gender_acc
                best_acc_race = race_acc
                best_acc_avg = epoch_avg_acc
                best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(model.state_dict(), 'models/classifier_{}.pth'.format(best_acc_avg))
                wandb.save('models/classifier_{}.pth'.format(best_acc_avg))

        wandb.log(metrics)
        
        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val accuracies:\nage_acc: {:.4f} gender_acc: {:.4f}  race_acc: {:.4f} '.format(best_acc_age, best_acc_gender, best_acc_race))

    # load best model weights
    model.load_state_dict(best_model_wts)
    torch.save(model.state_dict(), 'models/' + TRAINED_MODEL_FILE)
    wandb.save('models/' + TRAINED_MODEL_FILE)
    return model

if __name__ == "__main__":
    df_train = pd.read_csv('data/df_train.csv')
    df_val = pd.read_csv('data/df_val.csv')

    transform = {
        "train": transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ]),
        "val": transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
    }

    train_ds = FairFaceDataset(df_train, transform["train"])
    val_ds = FairFaceDataset(df_val, transform["val"])

    dataloaders_dict = {
        "train": DataLoader(train_ds, batch_size=BATCH_SIZE, shuffle=True, num_workers=2),
        "val": DataLoader(val_ds, batch_size=BATCH_SIZE, shuffle=True, num_workers=2)
    }

    dataset_sizes = {
        "train": len(df_train),
        "val": len(df_val)
    }

    efficientnet_b7 = models.efficientnet_b7(pretrained=True)
    """for param in model_resnet34.parameters():
        param.requires_grad = False
    for param in model_resnet34.fc.parameters():
        param.requires_grad = True"""

    model = MTCNN(efficientnet_b7, 0.1)

    model = model.to(device)

    summary(model, (3, 224, 224))

    lrmain = .0001

    optimizer = optim.Adam(model.parameters(), lr = lrmain, weight_decay = .00005)

    exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=5, gamma=0.1)
    
    model_final = train_model(model, optimizer, exp_lr_scheduler, num_epochs=NUM_EPOCHS)