import torch
#from torch import *
from torch.utils.data import DataLoader
from torchvision import transforms, models
from data_loader import FairFaceDataset
from model import MTCNN
import pandas as pd
import warnings
from constants import AGE_LABEL_MAP, AGE_NODES, BATCH_SIZE, GENDER_LABEL_MAP, GENDER_NODES, RACE_LABEL_MAP, RACE_NODES, TRAINED_MODEL_FILE

warnings.filterwarnings("ignore")
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def test_model_performance(model):

    model.eval()
    age_corrects = 0
    gender_corrects = 0
    race_corrects = 0

    age_predictions = []
    gender_predictions = []
    race_predictions = []

    print("Calculating accuracy on {} test samples".format(dataset_size_test))

    for inputs, (age, gender, race) in dataloader_test:
        inputs = inputs.to(device) 
        
        age = age.to(device)
        gender = gender.to(device)
        race = race.to(device)
        
        predicted = model(inputs)

        age_corrects += torch.sum(torch.max(predicted[0], 1)[1] == torch.max(age, 1)[1])
        gender_corrects += torch.sum(torch.max(predicted[1], 1)[1] == torch.max(gender, 1)[1])
        race_corrects += torch.sum(torch.max(predicted[2], 1)[1] == torch.max(race, 1)[1])

        age_predictions += torch.max(predicted[0], 1)[1].tolist()
        gender_predictions += torch.max(predicted[1], 1)[1].tolist()
        race_predictions += torch.max(predicted[2], 1)[1].tolist()

    age_acc = age_corrects.double() / dataset_size_test
    gender_acc = gender_corrects.double() / dataset_size_test
    race_acc = race_corrects.double() / dataset_size_test

    print()
    print(' '*14, 'Age', ' '*6, 'Gender', ' '*5, 'Race')
    print('-'*70)
    print('Accuracy', ' '*4, '{:.4f}'.format(float(age_acc)), ' '*4, '{:.4f}'.format(float(gender_acc)), ' '*4, '{:.4f}'.format(float(race_acc)))
    print()

    return age_predictions, gender_predictions, race_predictions

if __name__ == "__main__":
    df_test = pd.read_csv('data/df_test.csv')
    transform_test = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

    test_ds = FairFaceDataset(df_test, transform_test)
    dataloader_test = DataLoader(test_ds, batch_size=BATCH_SIZE, shuffle = False, num_workers=2)
    dataset_size_test = len(test_ds)

    efficientnet_b7 = models.efficientnet_b7(pretrained=False)

    model = MTCNN(efficientnet_b7, 0.1)

    model.load_state_dict(torch.load('models/' + TRAINED_MODEL_FILE))

    model = model.to(device)

    age_predictions, gender_predictions, race_predictions = test_model_performance(model)

    df_test['age_predicted'] = age_predictions
    df_test['gender_predicted'] = gender_predictions
    df_test['race_predicted'] = race_predictions

    df_test = df_test[['file', 'age', 'age_predicted', 'gender', 'gender_predicted', 'race', 'race_predicted']]

    print("Age Group wise accuracy:\n")    
    for i in range(0, AGE_NODES):
        df_gage = df_test[df_test['age']==i]
        sample_nums = len(df_gage)
        gender_acc = sum([1 for x,y in zip(list(df_gage['gender']), list(df_gage['gender_predicted'])) if x==y])/float(sample_nums)
        race_acc = sum([1 for x,y in zip(list(df_gage['race']), list(df_gage['race_predicted'])) if x==y])/float(sample_nums)
        print(' '*15, AGE_LABEL_MAP[i])
        print('-'*50)
        print(' '*14, 'Gender', ' '*5, 'Race')
        print('-'*50)
        print('Accuracy', ' '*4, '{:.4f}'.format(float(gender_acc)), ' '*5, '{:.4f}'.format(float(race_acc)))
        print()
    
    print("Gender wise accuracy:\n")    
    for i in range(0, GENDER_NODES):
        df_ggender = df_test[df_test['gender']==i]
        sample_nums = len(df_ggender)
        age_acc = sum([1 for x,y in zip(list(df_ggender['age']), list(df_ggender['age_predicted'])) if x==y])/float(sample_nums)
        race_acc = sum([1 for x,y in zip(list(df_ggender['race']), list(df_ggender['race_predicted'])) if x==y])/float(sample_nums)
        print(' '*15, GENDER_LABEL_MAP[i])
        print('-'*50)
        print(' '*14, 'Age', ' '*8, 'Race')
        print('-'*50)
        print('Accuracy', ' '*4, '{:.4f}'.format(float(age_acc)), ' '*4, '{:.4f}'.format(float(race_acc)))
        print()

    print("Race wise accuracy:\n")    
    for i in range(0, RACE_NODES):
        df_grace = df_test[df_test['race']==i]
        sample_nums = len(df_grace)
        age_acc = sum([1 for x,y in zip(list(df_grace['age']), list(df_grace['age_predicted'])) if x==y])/float(sample_nums)
        gender_acc = sum([1 for x,y in zip(list(df_grace['gender']), list(df_grace['gender_predicted'])) if x==y])/float(sample_nums)
        print(' '*15, RACE_LABEL_MAP[i])
        print('-'*50)
        print(' '*14, 'Age', ' '*6, 'Gender')
        print('-'*50)
        print('Accuracy', ' '*4, '{:.4f}'.format(float(age_acc)), ' '*4, '{:.4f}'.format(float(gender_acc)))
        print()

    df_test.to_csv('test_results.csv')