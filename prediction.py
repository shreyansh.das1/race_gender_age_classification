import torch.nn.functional as F
import torch
from torchvision import models, transforms
from torch.utils.data import Dataset, DataLoader
from constants import AGE_LABEL_MAP, RACE_LABEL_MAP, GENDER_LABEL_MAP, TRAINED_MODEL_FILE, INFERENCE_BATCH_SIZE
from model import MTCNN
from PIL import Image
import dlib
import os

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
dlib.DLIB_USE_CUDA = True

class InferenceDataset(Dataset):
    """
    Creates class for batch inference dataset
    """
    def __init__(self, imgs, transform):
        self.paths = imgs
        self.transform = transform

    def __len__(self): 
        return len(self.paths)

    def __getitem__(self,idx):
        img = Image.open(self.paths[idx]).convert('RGB')
        img = self.transform(img)
        return img

class RGAClassifier():
    """
    Creates the class used for inference from the model
    """
    def __init__(self):
        efficientnet_b7 = models.efficientnet_b7(pretrained=False)
        self.model = MTCNN(efficientnet_b7, 0.1)
        self.model.load_state_dict(torch.load('models/' + TRAINED_MODEL_FILE))
        self.model = self.model.to(device)
        self.transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])

    def detect_face(self, image_paths,  SAVE_DETECTED_AT='close_cropped', default_max_size=800, size = 300, padding = 0.25):
        cnn_face_detector = dlib.cnn_face_detection_model_v1('dlib_models/mmod_human_face_detector.dat')
        sp = dlib.shape_predictor('dlib_models/shape_predictor_5_face_landmarks.dat')
        base = 2000  # largest width and height
        empty_imgs = []
        for index, image_path in enumerate(image_paths):
            img = dlib.load_rgb_image(image_path)

            old_height, old_width, _ = img.shape

            if old_width > old_height:
                new_width, new_height = default_max_size, int(default_max_size * old_height / old_width)
            else:
                new_width, new_height =  int(default_max_size * old_width / old_height), default_max_size
            img = dlib.resize_image(img, rows=new_height, cols=new_width)

            dets = cnn_face_detector(img, 1)
            num_faces = len(dets)
            if num_faces == 0:
                empty_imgs.append(image_path)
                print("Sorry, there were no faces found in '{}'".format(image_path))
                continue
            # Find the 5 face landmarks we need to do the alignment.
            faces = dlib.full_object_detections()
            for detection in dets:
                rect = detection.rect
                faces.append(sp(img, rect))
            images = dlib.get_face_chips(img, faces, size=size, padding = padding)
            for idx, image in enumerate(images):
                img_name = image_path.split("/")[-1]
                path_sp = img_name.split(".")
                face_name = os.path.join(SAVE_DETECTED_AT,  path_sp[0] + "_" + "face" + str(idx) + "." + path_sp[-1])
                dlib.save_image(image, face_name)
        return empty_imgs

    def preprocess(self, images, SAVE_DETECTED_AT='close_cropped'):
        empty_imgs = self.detect_face(images)
        imgs = [os.path.join(SAVE_DETECTED_AT, x) for x in os.listdir(SAVE_DETECTED_AT) if (x.endswith(".jpg") or x.endswith(".png"))]
        ds = InferenceDataset(imgs, self.transform)
        dataloader = DataLoader(ds, batch_size=INFERENCE_BATCH_SIZE, shuffle = False, num_workers=2)
        return dataloader, empty_imgs

    def predict(self, images, SAVE_DETECTED_AT='close_cropped'):
        dataloader, empty_imgs = self.preprocess(images)
        self.model.eval()
        predictions = {}
        age_predictions = []
        gender_predictions = []
        race_predictions = []
        for inputs in dataloader:
            inputs = inputs.to(device)

            predicted = self.model(inputs)

            age_predictions += torch.max(predicted[0], 1)[1].tolist()
            gender_predictions += torch.max(predicted[1], 1)[1].tolist()
            race_predictions += torch.max(predicted[2], 1)[1].tolist()

        for idx, img in enumerate(images):
            if img in empty_imgs:
                predictions[img] = {'Age': 'N/A', 'Gender': 'N/A', 'Race': 'N/A'}
            else:
                predictions[img] = {'Age': AGE_LABEL_MAP[age_predictions[idx]], 'Gender': GENDER_LABEL_MAP[gender_predictions[idx]], 'Race': RACE_LABEL_MAP[race_predictions[idx]]}

        for f in os.listdir(SAVE_DETECTED_AT):
            if (f.endswith(".jpg") or f.endswith(".png")):
                os.remove(os.path.join(SAVE_DETECTED_AT, f))
        
        return predictions


