# Race, Gender and Age classification from the FairFace Dataset


## This branch holds the code to the following model for this task


### EfficientNet_B7 + 3FC Layers -> 3 branched classification heads


## Data CSVs

CSV files containing paths to dataset images and their respective labels are present in the data directory


## Run summary:

*Trained Model*: models/rga_classifier.pth

#### Performance on the training set (86744 samples)

Age_accuracy: 0.5146

Gender_accuracy: 0.9349

Race_accuracy: 0.6559

#### Performance on the validation set (5477 samples)

Age_accuracy: 0.5264

Gender_accuracy: 0.9319

Race_accuracy: 0.6569

#### Performance on the test set (5477 samples)

Age_accuracy: 0.5145

Gender_accuracy: 0.9303

Race_accuracy: 0.6529
