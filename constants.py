#Dictionary that maps the age label to the age bucket name (Present in the ratio 2:12:11:30:22:12:7:3:1)
AGE_LABEL_MAP = {0: '0-2', 1: '3-9', 2: '10-19', 3: '20-29', 4: '30-39', 5: '40-49', 6: '50-59', 7: '60-69', 8: 'more than 70'}

#Number of age buckets
AGE_NODES = len(AGE_LABEL_MAP)

#Dictionary that maps the gender label to the gender name (Present in the ratio 53:47)
GENDER_LABEL_MAP = {0: 'Male', 1: 'Female'}

#Number of gender
GENDER_NODES = len(GENDER_LABEL_MAP)

#Dictionary that maps the race label to the race name (Present in the ratio 14:14:14:19:11:15:13)
RACE_LABEL_MAP = {0: 'East Asian', 1: 'Indian', 2: 'Black', 3: 'White', 4: 'Middle Eastern', 5: 'Latino_Hispanic', 6: 'Southeast Asian'}

#Number of races
RACE_NODES = len(RACE_LABEL_MAP)

#Batch size
BATCH_SIZE = 8

#Batch size for inference from the model
INFERENCE_BATCH_SIZE = 8

#Number of epochs
NUM_EPOCHS = 20

#File name for the trained model
TRAINED_MODEL_FILE = 'rga_classifier.pth'